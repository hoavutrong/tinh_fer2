pip install -r requirements.txt

#1. copy fer2013.csv into data/

# tạo 7 tập dữ liệu từ khuôn mặt
python face_lanmark_detect.py

# đưa dữ liệu về dạng h5py theo format của proj này
python preprocess_fer2013.py

# train 7 models
python mainpro_FER.py


# kết hợp 7 models
python eval_7_models.py