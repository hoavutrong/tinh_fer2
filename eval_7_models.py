import os
import transforms as transforms
import torch
torch.cuda.empty_cache()
import torch.nn as nn
import numpy as np

from collections import defaultdict
from fer import FER2013
from models import *

cut_size = 44
use_cuda = torch.cuda.is_available()

models = [
        'left_eye', 'right_eye', 
        'left_cheek', 'right_cheek',
        'mouth', 'nose', 'forhead'
    ]

for testset in ('PublicTest', 'PrivateTest'):

    output_dict = defaultdict(list)
    target_dict = defaultdict(list)

    for model_name in models:
        print("model: ", model_name)

        # Data
        print('==> Preparing data..')
        transform_test = transforms.Compose([
            transforms.TenCrop(cut_size),
            transforms.Lambda(lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),
        ])

        
        dataset = FER2013(model_name=model_name, split = testset, transform=transform_test)
        dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False, num_workers=0)

        # load model
        print('==> Resuming from checkpoint..')
        path = 'FER2013_VGG19'
        assert os.path.isdir(path), 'Error: no checkpoint directory found!'
        checkpoint = torch.load(os.path.join(path,'{}_model_{}.t7'.format(testset, model_name,)))  #map_location=torch.device('cpu')

        net = VGG('VGG19')
        net.load_state_dict(checkpoint['net'])
        if use_cuda:
            net.cuda()
        net.eval()
        if testset == "PublicTest":
            print("{} best_{}_acc: {}".format(model_name, testset, checkpoint['acc']))    
        else:
            print("{} best_{}_acc: {}".format(model_name, testset, checkpoint['best_{}_acc'.format(testset)]))
        soft = nn.Softmax(dim=1)

        for batch_idx, (inputs, targets) in enumerate(dataset_loader):
            if targets.item() > 20:
                #noprint(batch_idx, " no data appended 0s")
                output_dict[batch_idx].append([[0]*7])
                target_dict[batch_idx].append(targets.item() - 20)
            else:
                import gc
                gc.collect()
                bs, ncrops, c, h, w = np.shape(inputs)
                inputs = inputs.view(-1, c, h, w)
                if use_cuda:
                    inputs, targets = inputs.cuda(), targets.cuda()
                inputs, targets = Variable(inputs, volatile=True), Variable(targets)
                outputs = net(inputs)
                outputs_avg = outputs.view(bs, ncrops, -1).mean(1)  # avg over crops
                probs = soft(outputs_avg).detach().cpu().numpy().tolist()
                output_dict[batch_idx].append(probs)
                target_dict[batch_idx].append(targets.item())


    total = 0
    correct = 0
    for i in range(len(dataset_loader)):
        final_output = np.stack(output_dict[i], axis=1)  # batch_size, 7, 7
        final_output = np.mean(final_output, axis=1)
        predicted = np.argmax(final_output, axis=1)
        targets = target_dict[i][0]
        total += 1
        correct += np.sum(np.equal(predicted, targets))

    acc = 100.*correct/total
    print("%s_acc: %0.3f" % (testset, acc))

