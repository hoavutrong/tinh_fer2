import csv
import cv2
import os
import sys
import numpy as np
from mtcnn import MTCNN
from PIL import Image, ImageDraw, ImageFont
from collections import defaultdict


data_dir = os.path.dirname(os.path.abspath(__file__)) + "/data"


def resize(img):
    return cv2.resize(img, (48, 48), interpolation = cv2.INTER_AREA)


def empty_img():
    return np.zeros(48*48, dtype=int)


def extract_face_segments(detector, img):
    img_size = img.shape[0]
    eye_width = int(img.shape[0]/10)
    mouth_height = int(img.shape[0]/10)
    anns = detector.detect_faces(img)
    if not anns:
        return None
        # return tuple ( [empty_img()] * 7 + [img[:,:,0]])
    for ann in anns:
        box = ann['box']
        for i in box:
            if i > img.shape[0]:
                i = img.shape[0]
            if i < 0:
                i = 0
        left_eye = ann['keypoints']['left_eye']
        right_eye = ann['keypoints']['right_eye']
        nose = ann['keypoints']['nose']
        mouth_left = ann['keypoints']['mouth_left']
        mouth_right = ann['keypoints']['mouth_right']
        keypoints = [
            left_eye, right_eye,
            mouth_left, mouth_right,
            nose
        ]

        # # fix the keypoints a bit
        # for idx, k in enumerate(keypoints):
        #     if k[0] < 0:
        #         keypoints[idx] = 
        #         k[0] = 0
        #     if k[1] > img.shape[0]:
        #         k[1] = img.shape[0]

        left_eye_img = img[
            max(0, left_eye[1] - eye_width): min(left_eye[1]+ eye_width, img_size),
            max(0, left_eye[0] - eye_width): min(img_size, left_eye[0]+ eye_width),
            :
        ]
        right_eye_img = img[
            max(0, right_eye[1] - eye_width): min(img_size, right_eye[1]+ eye_width),
            max(0, right_eye[0] - eye_width): min(img_size, right_eye[0]+ eye_width),
            :
        ]

        mouth_mid = mouth_left[1] + (mouth_right[1] - mouth_left[1])/2
        mouth_mid = int(mouth_mid)
        mouth_img = img[
            max(0, mouth_mid - mouth_height): min(img_size, mouth_mid + mouth_height),
            max(0, mouth_left[0] - 2): min(img_size, mouth_right[0] + 2),
            :
        ]

        # mui lay tu trung diem 2 mat xuong
        eye_mid0 = int(left_eye[0] + (right_eye[0] - left_eye[0])/2)
        eye_mid1 = int(left_eye[1] + (right_eye[1] - left_eye[1])/2)

        nose_img = img[
            max(0, eye_mid1 - 2): min(img_size, nose[1] + 4), # dai
            max(0, eye_mid0 - 3) : min(img_size, nose[0] + 3), # rong
            :
        ]

        # ma trai
        left_cheek_img = img[
            left_eye[1] + 2: mouth_left[1], # dai
            max(0, mouth_left[0] -8) : mouth_left[0], # rong
            :
        ]

        # ma phai
        right_cheek_img = img[
            right_eye[1] + 2: mouth_right[1], # dai
            mouth_right[0]: min(img.shape[0], mouth_right[0] + 8),
            :
        ]

        # forhead
        forhead_img = img[
            max(0, eye_mid1 - 12): (eye_mid1 - 2), # dai
            max(0, left_eye[0]-4): min(img_size, right_eye[0] + 4), # rong
            :
        ]

        cuts = [
            left_eye_img,
            right_eye_img,
            left_cheek_img,
            right_cheek_img,
            mouth_img,
            nose_img,
            forhead_img,
            img
            ]
        # for img in cuts:
        #     if img is None or img.shape[0] < 4 or img.shape[1] < 4:
        #         return None
        out_puts = []
        for img in cuts:
            if img is None or img.shape[0] < 4 or img.shape[1] < 4:
                out_puts.append(empty_img())
            else:
                out_puts.append(img[:,:,0])
        return tuple(out_puts)

         

if __name__ == "__main__":
    models = [
        'left_eye', 'right_eye', 
        'left_cheek', 'right_cheek',
        'mouth', 'nose', 'forhead', 'all'
    ]
    new_data = defaultdict(list)
    detector = MTCNN(min_face_size=18)
    no_face_detected = 0

    with open(data_dir + "/fer2013.csv",'r') as csvin:
        data = csv.reader(csvin)
        next(data, None)
        for idx, row in enumerate(data):
            pixels = [int(p) for p in row[1].split( )]
            img = np.asarray(pixels).reshape(48, 48)
            img = img[:, :, np.newaxis]
            img = np.concatenate((img, img, img), axis=2).astype("uint8")
            cuts = extract_face_segments(detector, img)
            if cuts:
                for idx, cut in enumerate(cuts):
                    if cut.any():
                        cut = resize(cut).reshape(48*48)
                        pixels = " ".join([str(p) for p in cut.tolist()])
                        row_copy = (row[0], pixels, row[2])
                    else:
                        no_face_detected +=1
                        row_copy = (20 + int(row[0]), " ".join(['0']*2304), row[2])
                    
                    new_data[models[idx]].append(row_copy)
            
            else:
                no_face_detected +=1
            # if idx > 200:
            #     break

    for model in models:
        with open(data_dir + "/fer2013_{}.csv".format(model,),'w') as out:
            csv_out=csv.writer(out)
            csv_out.writerow(['emotion', 'pixels', 'Usage'])
            #print(new_data[model])
            csv_out.writerows(new_data[model])

    print("Cannot detect faces in {} images".format(no_face_detected))
            
